import {Component, Fragment } from 'react'
import ReactPeople from "../../datas/ReactCoreTeam.json";
import Card from "./card"

class SocialNetworkDeux extends Component
{
    constructor(props)
    {
      super(props);
      this.state=
      {
        Nom:'Person',
        Prenom :'Mike'
      };
    }


    render()
    {
      const{ Nom, Prenom} = this.state;
        return(
            <Fragment>
            <h3>The Social Network - {Nom}</h3>
            {ReactPeople.map((profile) => (
              <Card  key={profile.id}  data={profile} />
            ))}
          </Fragment>   
        
            );
    }
}

export default SocialNetworkDeux