import React from "react"; 
import style from "./SocialNetwork.module.css"; 

const Card=function(props)
{
    return(
        <div className={style.SocialCard}>
        <img className={style.SocialImg} src={props.data.avatar_url} alt={props.data.avatar_url} />
        <div className={style.SocialInfo}>
          <p className={style.SocialName}>{props.data.name}</p>
          <pre>Company</pre>
          <p className={style.SocialCompany}>{props.data.company}</p>
          <pre>Location</pre>
          <p className={style.SocialCompany}>{props.data.location}</p>
          <pre>Bio</pre>
          <p className={style.SocialCompany}>{props.data.bio}</p>
          <pre>Followings</pre>
          <p className={style.SocialCompany}>{props.data.following}</p>
          <pre>Followers</pre>
          <p className={style.SocialCompany}>{props.data.followers}</p>
        </div>
      </div>      
    
        );
}

export default Card;