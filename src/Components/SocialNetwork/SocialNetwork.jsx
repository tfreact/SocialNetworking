import { Fragment } from 'react'
import ReactPeople from "../../datas/ReactCoreTeam.json"; 
import Card from "./card"


const SocialNetwork=function(props)
{    
   
    return(
        <Fragment>
           
        <h3>The Social Network</h3>
        {ReactPeople.map((profile) => (
          <Card  key={profile.id}  data={profile} />
        ))}
      </Fragment>   
    
        );
}

export default SocialNetwork;